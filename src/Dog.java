public class Dog {

//    1.Create class Dog.
//    a)Add private fields to the class , like name , age , gender , race, weigth
//    b)Create constructor that accepts all of the class fields
//    c)Create additional constructor , that will accept only gender and race. It should call main
//    constructor with default values
//    d)Create getters and setters for age and weigth
//    e)Create object of class Dog. Verify if everything works as expected
//    f)Add verification for all arguments passed to the setters . E.g . setWeigth method should
//    not accept values below or equal to 0.

    private String name, race;
    private int age;
    private boolean gender;
    private double weight;

    public Dog (String name, double weight,int age) {
        this.name = name;
        this.weight = weight;
        this.age = age;
        this.gender = true;
        this.race = "Dog german";


    }
    public Dog(boolean gender, String race){
        this ("Gigi",65.0, 24);
        this.gender = gender;
        this.race = race;

    }
    public Dog(){
        this.name ="Florin";
        this.race = "Shitzu";
        this.age = 27;
        this.gender = true;
        this.weight = 84;

    }

    public String getName () {
        return name;
    }

    public String getRace () {
        return race;
    }

    public int getAge () {
        return age;
    }

    public boolean getGender () {
        return gender;
    }

    public String isGender(){
        if(gender){
           return "Male";
        }
        else{
            return "Female";
        }

    }

    public double getWeight () {
        return weight;
    }

    public void setName (String name) {
        this.name = name;
    }

    public void setRace (String race) {
        this.race = race;
    }

    public void setAge (int age) {
        this.age = age;
    }

    public void setGender (boolean gender) {
        this.gender = gender;
    }

    public void setWeight (double weight) {
        if (weight>0 && weight<=200) {
            this.weight = weight;
        }
            else{
                System.out.println ("Valoarea introdusa nu corespunde intervalului cerut");
            }
        }

    public void printMethod(){
        System.out.println ();
        System.out.println ("Name: " +  getName ());
        System.out.println ("Age: " + getAge ());
        System.out.println ("Gender: " + isGender ());
        System.out.println ("Race: "+ this.race);
        System.out.println ("Weight: " + this.weight);

    }
}
