public class Pocket {
//    a)Add field „money”, create constructor, getter and setter.
//    b) Add verification for both getter and setter. Getter should result in returning as much
//    money, as the user asked for. It should return 0 if money <= 10.
//    c) Setter should not accept values below 0 and greater than 3000. It may print a message
//    like „I don’t have enough space in my pocket for as much money!”

    private double money;

    public Pocket(double money){
        this.money = money;
    }

   public Pocket(){
   }

    public void setMoney (double money) {
        if(money<0 || money> 3000){
            System.out.println ("You don't have enough space in your pocket for as much money.");
        } else{
            this.money = money;

        }
    }

    public double getMoney () {
        if (money < 10) {
            return 0;
        } else {
            return money;
        }

    }


    public void afisare(){
        System.out.println ("You have " + money + " in your pocket.");

    }
    public void addMoneyToPocket(double dinero){
    if(money + dinero > 3000){
        System.out.println ("You don't have enough space in your pocket for as much money.");
    }
    else if(money + dinero <= 0) {
        System.out.println ("You haven't any money");

    }
    else{
        this.money += dinero;
        }

    }

}


