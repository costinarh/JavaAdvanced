public class Main {
    public static void main (String[] args) {

        Dog dog = new Dog("Gigi", 40.0,  10);
        dog.printMethod ();

        Dog dog2 = new Dog(false, "Buldog");
        dog2.printMethod ();

        Dog dog1 = new Dog();
        dog1.printMethod ();

        dog.setWeight (201.0);
        System.out.println (dog.getWeight ());


        dog.setGender (false);
        System.out.println (dog.isGender ());


        Pocket portofel = new Pocket();
        System.out.println ("Varianta 1 " + portofel.getMoney ());
        System.out.println ();
        portofel.setMoney(150);
        System.out.println ("Varianta 2 " + portofel.getMoney ());
        portofel.setMoney (3200);
        System.out.println ("Varianta 3 " +portofel.getMoney ());
        System.out.println ();

        portofel.addMoneyToPocket (3000);
        portofel.afisare ();
        System.out.println ();

        portofel.addMoneyToPocket (0);
        portofel.afisare ();
        System.out.println ();

        portofel.addMoneyToPocket (1000);
        portofel.afisare ();
        System.out.println ();
        portofel.addMoneyToPocket (-2000);
        portofel.afisare ();


    }

}
